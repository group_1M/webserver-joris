<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 25-11-2016 18:34
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="UTF-8"/>
    <title>Zodiac signs</title>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
</head>
<body>
<main class="container">

    <div class="jumbotron">
        <form action="process_zodiac_feedback.php" method="post">

            <div class="col-12">

                <label for="message_date" class="col-2">
                    Message date
                </label>
                <input type="date" id="message_date" name="message_date" class="col-2" placeholder="2016-11-12"/>

                <label for="message_time" class="col-2">
                    Message time
                </label>
                <input type="time" id="message_time" name="message_time" class="col-2" placeholder="10:30"/>

            </div>

            <div class="col-12">

                <label for="sender" class="col-2">
                    Name
                </label>
                <input type="text" id="sender" name="sender" class="col-2" placeholder="Name"/>

                <label for="message" class="col-2">
                    message
                </label>
                <input type="text" id="message" name="message" class="col-2" placeholder="Message"/>
            </div>

            <div class="col-12">
                <label for="public_message" class="col-2">
                    public message
                </label>
                <input type="text" id="public_message" name="public_message" class="col-2" placeholder="Y/N"/>
            </div>

            <br>
            <button class="" type="submit">Send message</button>
        </form>
        <a href="viewZodiac.php">View all messages</a>
    </div>
</main>

</body>
</html>

<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 27-11-2016 15:03
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */
declare(strict_types = 1);

if( php_sapi_name() == 'cli' )
{
    define('ROOT_DIR', __DIR__ . DIRECTORY_SEPARATOR);
    define('CONFIG_DIR', ROOT_DIR . 'config' . DIRECTORY_SEPARATOR);
    define('WEBSITE_DIR', ROOT_DIR . 'src' . DIRECTORY_SEPARATOR . 'Website' . DIRECTORY_SEPARATOR);
    define('RESOURCES_DIR', WEBSITE_DIR . 'Resources' . DIRECTORY_SEPARATOR);
    define('DEBUG', 1);

    require __DIR__ . DIRECTORY_SEPARATOR . "vendor/autoload.php";


    $application = new JorisRietveld\Website\Core\Application();
    $request = new Symfony\Component\HttpFoundation\Request(  );

    // Handle the request and send an response
    $application->handle( $request::create( 'php/StendenTwitter/console' ) );

    $doctrineEntityManager = $application->getDoctrineConsole();
}
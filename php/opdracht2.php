<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 17-11-2016 08:56
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

$array = [
    'Small Box' => [
        'Length' => 12,
        'Width' => 10,
        'Depth' => 3.5,
    ],
    'Medium Box' => [
        'Length' => 30,
        'Width' => 20,
        'Depth' => 4,
    ],
    'Large Box' => [
        'Length' => 60,
        'Width' => 40,
        'Depth' => 11.5,
    ],
];

function getArrayList( array $array ) : string
{
    ob_start();
    foreach( $array as $key => $value )
    {
        echo 'The volume of the ' . $key . ' is: ' . ( $value[ 'Length' ] * $value[ 'Width' ] * $value[ 'Depth' ] ).'<br />';
    }

    return ob_get_clean();
}
?>
<!DOCTYPE html>
<html lang="NL">
<head>
    <meta charset="UTF-8" />
    <title>Opdracht 2</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <h1>Opdracht 2</h1>
    <?= getArrayList( $array ) ?>
    <fieldset id="code">
        <legend>Source code: Opdracht2.php</legend>
        <?= highlight_file('opdracht2.php', true ) ?>
    </fieldset>
</body>
</html>

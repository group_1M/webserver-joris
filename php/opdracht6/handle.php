<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 19-11-2016 18:20
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */
require __DIR__.DIRECTORY_SEPARATOR.'handleOrder.php';


$getPosts = function () use ( $articles ) : array
{
    $formData = [];

    foreach ($articles as $article)
    {
        if ( !isset( $_POST[ 'quantity-' . $article[ 'name' ] ] ) )
        {
            return [];
        }

        $formData[ $article[ 'name' ] ] = [
            'quantity' => htmlentities( $_POST[ 'quantity-' . $article[ 'name' ] ], ENT_QUOTES, 'UTF-8' ),
            'price' => $article[ 'Price' ],
        ];
    }
    return $formData;
};

$writeToNotDatabase = function ( string $data, string $notDatabaseDir = '../OnlineOrders' )
{
    if ( !is_dir( $notDatabaseDir ) )
    {
        throw new LogicException( 'The folder passed in the $writeToNotDatabase closure does not exist.' );
    }
    $date = new DateTime();

    $fileName = 'Order ' . $date->format( 'd-m-Y H:i:s' );

    if ( $filePointer = fopen( $notDatabaseDir . DIRECTORY_SEPARATOR . $fileName . '.txt', 'x' ) )
    {
        fwrite( $filePointer, $data );
        fclose( $filePointer );
        return;
    }
    throw new ErrorException( 'Can\'t write data to the file: ' . $fileName . '. Does the file already exist?' );
};

$formatDataForStorage = function ( array $orderItems ) : string
{
    $output = sprintf( "Order\n\n" );
    $tableHeader = sprintf( "Product name\t| quantity\t| price\t\t| total price \t\n" );
    $tableLine = sprintf( "%s \n", str_repeat( '-', strlen( $tableHeader )+4 ));

    $output .= $tableHeader;
    $output .= $tableLine;
    $totalPrice = 0.0;

    foreach( $orderItems as $itemName => $itemDetails )
    {
        $subTotal = (float)$itemDetails['price'] * (float)$itemDetails['quantity'];
        $totalPrice += $subTotal;
        $output .= sprintf(
            "%s\t\t| %s\t\t| € %0.2f\t| %0.2f\t\n",
            ( $itemName . str_repeat( ' ', 9 - strlen( $itemName )) ),
            ( $itemDetails['quantity'] . str_repeat( ' ', 4 - strlen( $itemDetails[ 'quantity' ] )) ),
            (float)$itemDetails['price'],
            (float)$subTotal );
    }

    $output .= $tableLine;
    $output .= sprintf( "Total: \t %0.2f\n", (float)$totalPrice );
    $output .= $tableLine;

    return $output;
};

$handleFormSubmit = function () use ( $writeToNotDatabase, $formatDataForStorage, $getPosts ) : string
{
    $writeToNotDatabase( $formatDataForStorage( $getPosts() ) );
    return 'The order has successfully been submitted.';
};

if( count( $getPosts() ) > 0 )
{
    echo $handleFormSubmit();
}
else
{
    echo 'Something went wrong.';
    var_dump($_POST);
}
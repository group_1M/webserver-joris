<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 21-11-2016 19:34
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Controllers;


use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\Response;

class FileBugReport extends BaseController implements ControllerContract
{
    protected $requiredPostFields = [
        'product_name',
        'product_version',
        'hardware',
        'operating_system',
        'frequensy',
        'solutions'
    ];

    public function index( $message = "" ) : Response
    {
        return new Response(
            $this->renderWebpage( 'fileBugReport', [ 'message' => $message ] ),
            200
        );
    }

    public function handle() : Response
    {
        foreach ($this->requiredPostFields as $requiredPostField)
        {
            if ( !array_key_exists( $requiredPostField, $_POST ) )
            {
                return $this->index( '<b class="text-danger">You must fill in all fields also:' . $requiredPostField . '</b>' );
            }
        }
        try
        {
            $connection = $this->getConnection();

            $statement = $connection->prepare( 'INSERT INTO php2.bug_reports(product_name, product_version, hardware, operating_system, frequensy, solutions) VALUES( ?,?,?,?,?,? )' );
            $count = 1;
            foreach ($_POST as $columnKey => $postValue)
            {
                if ( in_array( $columnKey, $this->requiredPostFields, true ) )
                {
                    $statement->bindValue( $count, $postValue );
                    $count++;
                    //$statement->bindValue( $columnKey, $postValue );
                }
            }

            if ( $statement->execute() )
            {
                return $this->index( '<b class="text-success">Your bug report has been saved in the database.</b>' );
            }
        } catch ( \PDOException $e )
        {
            return $this->index( '<b class="text-danger">There was an error saving your bug in the database.</b><br>'.$e->getMessage() );
        }
    }
}
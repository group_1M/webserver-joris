<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 19-11-2016 14:45
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

require __DIR__ . DIRECTORY_SEPARATOR . 'opdracht6/handleOrder.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Opdracht 6</title>
    <link href="opdracht6/style.css" rel="stylesheet" />
    <script src="opdracht6/script.js"></script>
</head>
<body>
    <h1>Opdracht 6</h1>
    <hr />
    <?= $renderForm() ?>
    <hr />
</body>
</html


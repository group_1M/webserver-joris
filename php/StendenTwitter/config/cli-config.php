<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 27-11-2016 14:56
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once __DIR__. DIRECTORY_SEPARATOR . '../index_console.php';

return ConsoleRunner::createHelperSet( $doctrineEntityManager );
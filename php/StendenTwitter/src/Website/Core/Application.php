<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 16-09-2016 14:18
 */
declare(strict_types = 1);

namespace JorisRietveld\Website\Core;

use JorisRietveld\Website\Controllers\Console;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class Application
{
    protected $request;
    protected $response;
    protected $session;

    /**
     * Application constructor. This is the hart of the application.
     */
    public function __construct(  )
    {
        $this->response = new Response();
        $this->session = new Session();
        if( ! $this->session->isStarted() )
        {
            $this->session->start();
        }
    }

    /**
     *  Handle an incoming http request and return an appropriate response.
     *
     * @param Request $request
     * @return Response
     */
    public function handle( Request $request = null ) : self
    {
        $this->setRequest( $request );

        $this->getRequest()->setSession( $this->session );
        $route = $this->resolveRoute();

        try {
            $this->callController($route->getController(), $route->getMethod(), $route->getArguments() );
        }
        catch ( \Exception $exception )
        {
            $route->setController( 'Error500' );
            $route->setMethod( 'index' );
            $route->setArguments([ $exception ]);
            $this->callController($route->getController(), $route->getMethod(), $route->getArguments() );
        }
        return $this;
    }

    /**
     * When the request is handled send the response;
     * @return Response
     */
    public function send()
    {
        $this->response->prepare( $this->request );
        return $this->response->send();
    }
    /**
     * This method will call the controller matched by the route.
     *
     * @param $controller
     * @param $method
     * @return Response
     */
    protected function callController( string $controller, string $method, array $arguments = [] ) : Response
    {
        $controller = '\\JorisRietveld\\Website\\Controllers\\' . $controller;
        $controller = new $controller( $this->request );
        $controller->setRequest( $this->request );

        $response = $controller->{$method}( ...array_values( $arguments) );

        if( is_a($response, '\Symfony\Component\HttpFoundation\Response'))
        {
            $this->response = $response;
            return $this->response;
        }
        throw new \LogicException('The controller must return an Request object!');
    }

    /**
     * This Method will resolve the route to an controller based on the request.
     *
     * @param Request|null $request
     * @return Route
     */
    protected function resolveRoute( Request $request = null ) : Route
    {
        $routerResolver = new RouteResolver();
        return $routerResolver->resolve( $this->getRequest() );
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest( Request $request )
    {
        $this->request = $request ?: new Request();
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function getDoctrineConsole(  )
    {
        return ( new Console() )->getEntityManager();
    }


}
<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 19-11-2016 18:49
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

function getAllOrderFiles ( string $orderDirectory = '../OnlineOrders', array $allowedFileExtensions = [ 'txt' ] ) : array
{
    if( !is_dir( $orderDirectory ) )
    {
        throw new LogicException( 'The directory where the orders are stored: '. $orderDirectory . ' does not exist.' );
    }

    $directory = new RecursiveDirectoryIterator( $orderDirectory );
    $filter = new RecursiveCallbackFilterIterator(
        $directory,
        function ( $current, $key, $iterator ) use ( $allowedFileExtensions )
        {

            if ( $iterator->hasChildren() )
            {
                return true;
            }

            if ( in_array( $current->getExtension(), $allowedFileExtensions, false ) )
            {
                return true;
            }

            return false;
        }
    );

    $orders = [];

    foreach ( new IteratorIterator( $filter ) as $file )
    {

    }
}
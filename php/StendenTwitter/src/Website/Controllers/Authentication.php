<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 26-11-2016 04:45
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Controllers;


use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Entity\User;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Authentication extends BaseController implements ControllerContract
{
    protected $user;

    public function __construct( Request $request )
    {
        parent::__construct();

        $this->setRequest( $request );

        if ( $this->getRequest()->getSession()->has( 'user' ) )
        {
            $this->user = unserialize( $this->getRequest()->getSession()->get( 'user' ) );
        }
        else
        {
            $this->user = new User();
        }

        $this->user->setRepository(
            new \JorisRietveld\Website\Repository\User( $this->getConnection() )
        );
    }

    public function index() : Response
    {
        //dump($this->user);
        if ( $this->user->isAuthenticated() === false )
        {
            return $this->login();
        }
        else
        {
            return new Response(
                $this->renderWebpage( 'home', [
                    'user' => $this->user
                ] )
            );
        }
    }

    public function login() : Response
    {
        if ( $this->user->isAuthenticated() === true )
        {
            return $this->index();
        }
        else
        {
            return new Response(
                $this->renderWebpage( 'login' ),
                200
            );
        }
    }

    public function handleLogin() : JsonResponse
    {
        if ( $this->getRequest()->request->has( 'username' ) && $this->getRequest()->request->has( 'password' ) )
        {
            $this->authenticateUser(
                $this->getRequest()->request->get( 'username' ),
                $this->getRequest()->request->get( 'password' )
            );

            if ( $this->user->isAuthenticated() )
            {
                $message = 'OK';
            }
            else
            {
                $message = 'The combination of username and password was not found in the database.';
            }
        }
        else
        {
            $message = 'You must fill in all required fields.';
        }

        return new JsonResponse( [ 'message' => $message ] );
    }

    public function logout() : Response
    {
        $this->user->deAuthenticate();
        $this->getRequest()->getSession()->invalidate();
        return $this->login();
    }

    public function handleRegister() : JsonResponse
    {
        if (
            $this->getRequest()->request->has( 'username' ) &&
            $this->getRequest()->request->has( 'email' ) &&
            $this->getRequest()->request->has( 'password' ) &&
            $this->getRequest()->request->has( 'confirm-password' ) &&
            $this->getRequest()->files->has( 'file' )
        )
        {
            if ( $this->getRequest()->request->has( 'password' ) == $this->getRequest()->request->has( 'confirm-password' ) )
            {
                $this->user->setUsername(
                    $this->getRequest()->request->get( 'username' )
                );

                $this->user->setEmail(
                    $this->getRequest()->request->get( 'email' )
                );

                $this->user->setPasswordHash(
                    password_hash(
                        $this->getRequest()->request->get( 'email' ),
                        PASSWORD_BCRYPT
                    )
                );

                $fileName = 'avatar_' . $this->getRequest()->request->get( 'username' ) . '.' . $this->getRequest()->files->get( 'file' )->guessExtension();

                $this->getRequest()->files->get( 'file' )->move(
                    FILE_UPLOAD_PATH,
                    $fileName
                );

                $this->user->setImagePath( $fileName );

                $id = $this->user->insert();
                $this->user->setId( $id );
                $this->user->setAuthenticated( TRUE );
                $this->getRequest()->getSession()->set( 'user', serialize( $this->user ) );

                $message = 'OK';
            }
            else
            {
                $message = 'The passwords do not match.';
            }
        }
        else
        {
            $message = 'You must fill in all required fields.';
        }

        return new JsonResponse( [ 'message' => $message ] );
    }

    public function authenticateUser( string $username, string $password )
    {
        if ( $this->user->authenticate( $username, $password ) )
        {
            $this->getRequest()->getSession()->set( 'user', serialize( $this->user ) );
        }
    }
}
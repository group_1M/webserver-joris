<?php
require __DIR__ . DIRECTORY_SEPARATOR .'bootstrap.php';

$rssParser = new \JorisRietveld\RssAndAtom\RssParser();
?>
<!DOCTYPE html "Website van Torvalds hoogeschool">
<html lang="EN">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="cache-control" content="0"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
    <title>Torvalds hogeschool</title>
</head>
<body>

    <div class="page-wrapper container-fluid">

        <!-- The top header of the webpage with the logo and navigation of the website -->
        <header class="header-wrapper col-12">

            <!-- The header wrapper that contains the torvalds hogeschool logo -->
            <div class="header-left col-2">
                <img src="images/logo.png" alt="Torvalds hogeschool logo" class="brand-logo offset-2" />
            </div>

            <!-- The header wrapper that contains the sites navigation an language options -->
            <div class="header-right col-10">

                <!-- The wrapper that contains the sites navigation menu and search function -->
                <nav class="nav-wrapper col-12">
                    <ul>

                        <li class="">
                            <a href="">Opleidingen</a>
                        </li>

                        <li>
                            <a href="">Opendagen</a>
                        </li>

                        <li>
                            <a href="">Contact</a>
                        </li>

                        <li>
                            <a href="">Over Ons</a>
                        </li>

                        <li>
                            <label for="search"></label>
                            <input name="search" id="search" type="text" placeholder="Zoek"/>
                        </li>

                    </ul>
                </nav>
            </div>

            <!-- The wrapper that contains the buttons for switching the sites language -->
            <div class="lang-wrapper col-2 offset-8">

                    <a href="?lang=nl">
                        <img src="images/Netherlands-Flag-icon.png" alt="Dutch flag" class="flag-icon col-5"/>
                    </a>

                    <div class="flag-spacer col-1">
                        <div class="flag-spacer-item"></div>
                    </div>

                    <a href="?lang=en">
                        <img src="images/United-Kingdom-flag-icon.png" alt="English flag" class="flag-icon col-5"/>
                    </a>

                </div>

        </header>

        <!-- The full width image on the home page -->
        <div class="header-image-wrapper col-12">
            <img src="images/background.jpg" class="header-background-image" alt="People working on computer">
        </div>

        <main class="main-wrapper col-12">

            <section class="col-4">
                <h2 class="color-heading-2">Evenementen</h2>
                <br/>
                <address>
                    Open dag<br/>
                    19 November 2016<br/>
                    10:00<br/>
                    Rotterdam
                </address>
                <br/>
                <p class="widget-text">
                    Je vervolg studie kies je niet zomaar. Mischien heb je nog geen idee wat je wilt gaat studeren. Of mischien
                    weet je het het al, maar wil je graag wat meer informatie. Tijdens de open dag maken we graag kennis met jou.
                </p>
                <br />
                <br />
                <a href="?p=/evenementen"><h2 class="color-heading-2">Ga naar evenementen</h2></a>
            </section>

            <section class="col-4">
                <h2 class="color-heading-2">Nieuws</h2>
                <dl class="">
                <?php
                $feed = $rssParser->getFeed('http://feeds2.feedburner.com/tweakers/mixed', 10);

                foreach ( $feed->getNewsElements(3) as $newsElement )
                {
                    echo '<dt><a href="' . $newsElement->getLink() . '">' . $newsElement->getTitle() . '</a></dt>';
                    echo '<dd style="font-size: 14px;">' . substr( $newsElement->getDescription(), 0,140) . '...</dd>';
                    echo '<br/>';
                }
                ?>
                </dl>
            </section>

            <section class="col-4">
                <h2 class="color-heading-2">Locaties</h2>
            </section>

        </main>
    </div>
    <footer class="footer-wrapper col-12">

        <a href="" class="footer-links">
            <h3>Contact</h3>
        </a>

        <h3 class="footer-links">|</h3>

        <a href="" class="footer-links">
            <h3>Colofon</h3>
        </a>

        <h3 class="footer-links">|</h3>

        <a href="" class="footer-links">
            <h3>Sitemap</h3>
        </a>

    </footer>
</body>
</html>
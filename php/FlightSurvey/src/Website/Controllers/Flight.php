<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 23-11-2016 00:26
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Controllers;


use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\Response;

class Flight extends BaseController implements ControllerContract
{
    protected $getFlight = ' SELECT 
                                `id`, 
                                `flight_time`, 
                                `flight_number`, 
                                `flight_orgin`, 
                                `flight_destenation` 
                              FROM `flight`';

    protected $getFlightById = 'SELECT 
                                    `id`, 
                                    `flight_time`, 
                                    `flight_number`, 
                                    `flight_orgin`, 
                                    `flight_destenation` 
                                  FROM `flight` 
                                  WHERE `id` = :id';

    protected $insertFlight = 'INSERT INTO 
                                  `flight`(
                                    `flight_time`, 
                                    `flight_number`, 
                                    `flight_orgin`, 
                                    `flight_destenation`
                                    ) 
                                  VALUES( 
                                    :flight_time, 
                                    :flight_number, 
                                    :flight_orgin, 
                                    :flight_destenation 
                                  )';

    protected $updateFlight = 'UPDATE `flight`
                                SET 
                                `flight_time`=:flight_time, 
                                `flight_number`=:flight_number, 
                                `flight_time`=:flight_time, 
                                `flight_orgin`=:flight_orgin, 
                                `flight_destenation`=:flight_destenation
                                WHERE `id`=:id';

    protected $requiredParametersInsert = [
        'flight_time',
        'flight_number',
        'flight_orgin',
        'flight_destenation',
    ];

    protected $requiredParametersUpdate = [
        'flight_time',
        'flight_number',
        'flight_orgin',
        'flight_destenation',
        'id',
    ];

    public function index()
    {
        return new Response(
            $this->renderWebpage( 'viewFlight', [
                'flights' => $this->getFlightsFromDatabase()
            ] ),
            200
        );
    }

    public function insertFlight()
    {
        if ( count( $_POST ) > 0 )
        {
            return new Response(
                $this->renderWebpage( 'insertFlight', [
                    'message' => $this->insertFlightInDatabase( $_POST ),
                ] ),
                200
            );

        }
        return new Response(
            $this->renderWebpage( 'insertFlight', [

            ] ),
            200
        );
    }

    public function insertFlightInDatabase( array $values )
    {
        foreach ($this->requiredParametersInsert as $requiredParameterInsert)
        {
            if ( !array_key_exists( $requiredParameterInsert, $_POST ) )
            {
                return '<b class="text-danger">You must fill in all fields also:' . $requiredParameterInsert . '</b>';
            }
        }
        try
        {
            $statement = $this->getConnection()->prepare( $this->insertFlight );

            foreach ($_POST as $columnKey => $postValue)
            {
                if ( in_array( $columnKey, $this->requiredParametersInsert ) )
                {
                    $statement->bindValue( ':' . $columnKey, $postValue );
                }
            }

            if ( $statement->execute() )
            {
                return '<b class="text-success">The flight has been saved in the database.</b>';
            }

        } catch ( \PDOException $e )
        {
            return '<b class="text-danger">There was an error saving the flight in the database.</b><br>' . $e->getMessage();
        }

        $statement = $this->getConnection()->prepare( $this->insertFlight );

        return $statement->execute( $values );
    }

    public function updateFlightInDatabase( array $updateValues ) : bool
    {
        $statement = $this->getConnection()->prepare( $this->updateFlight );
        return (bool)$statement->execute( $updateValues );
    }

    public function getFlightFromDatabase( int $flightId )
    {
        $statement = $this->getConnection()->prepare( $this->getFlightById );
        $statement->bindValue( ':id', $flightId, \PDO::PARAM_INT );
        $statement->execute();
        return $statement->fetchAll();
    }

    public function getFlightsFromDatabase()
    {
        $statement = $this->getConnection()->prepare( $this->getFlight );
        $statement->execute();
        return $statement->fetchAll( \PDO::FETCH_ASSOC );
    }
}
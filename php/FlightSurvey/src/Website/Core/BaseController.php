<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 17-09-2016 12:12
 */
declare(strict_types = 1);

namespace JorisRietveld\Website\Core;


use JorisRietveld\Website\ThirdParty\Weather;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use JorisRietveld\Website\Helper\TemperatureConverter;

abstract class BaseController
{
    protected $twigEnv;
    protected $databaseConnection;

    public function __construct()
    {
        $twigLoader = new \Twig_Loader_Filesystem( RESOURCES_DIR . 'twig' . DIRECTORY_SEPARATOR );
        $this->twigEnv = new \Twig_Environment( $twigLoader, [
            'debug' => true,
        ]);

        $this->twigEnv->addExtension( new \Twig_Extension_Debug() );

        $this->databaseConnection = (new Database() )->connect();
    }

    /**
     * @param $fileName
     * @return string
     */
    public function loadedTemplate( $fileName )
    {
        $templateFile = RESOURCES_DIR . 'views' . DIRECTORY_SEPARATOR . $fileName;

        if( file_exists( $templateFile ))
        {
            return file_get_contents( $templateFile );
        }
        throw new FileNotFoundException('The template file:' . $fileName . ' is not found!');
    }

    public function getConfiguration( $for )
    {
        $config = new ConfigLoader();
        return $config->get($for);
    }

    public function renderWebpage( string $webpageName, array $context = [] )
    {
        return $this->twigEnv->render( $webpageName.'.html.twig', $context );
    }

    public function getConnection(  )
    {
        return $this->databaseConnection;
    }
}
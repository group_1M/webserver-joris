<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 26-11-2016 09:28
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Entity;


use JorisRietveld\Website\Core\Application;
use JorisRietveld\Website\Helper\WhereClause;
use JorisRietveld\Website\Interfaces\EntityInterface;
use JorisRietveld\Website\Repository\User as UserRepository;

class User implements EntityInterface, \Serializable 
{
    protected $id;
    protected $username;
    protected $passwordHash;
    protected $email;
    protected $imagePath;
    protected $authenticated = FALSE;
    protected $repository;

    public function __construct( )
    {

    }

    public function authenticate( string $username, string $password ) : User
    {
        $users = $this->repository->getAllWithFilter( '`stenden_users`.`username`=:username', [
            ':username' => $username
        ] );

        if( count( $users ) < 1 )
        {
            return $this;
        }

        $user = $users[0];

        if( password_verify( $password, $user->passwordHash ))
        {
            $this->setId( $user->id );
            $this->setUsername( $user->username );
            $this->setPasswordHash( $user->passwordHash );
            $this->setEmail( $user->email );
            $this->setImagePath( $user->imagePath );
            $this->authenticated = TRUE;
        }
        return $this;
    }

    public function insert(  ) : int
    {
        return $this->repository->insert( $this );
    }

    public function deAuthenticate(  )
    {
        $this->setId( 0 );
        $this->setUsername( '' );
        $this->setPasswordHash( '' );
        $this->setEmail( '' );
        $this->setImagePath( '' );
        $this->authenticated = FALSE;
    }

    public function setRepository( UserRepository $repository ) : User
    {
        $this->repository = $repository;
        return $this;
    }

    public function getRepository(  ) : UserRepository
    {
        return $this->repository;
    }

    public function isAuthenticated() : bool
    {
        return $this->authenticated;
    }

    public function setAuthenticated( bool $authenticated )
    {
        $this->authenticated = $authenticated;
    }

    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId( int $id ) : User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername( string $username ) : User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash() : string
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     * @return User
     */
    public function setPasswordHash( string $passwordHash ) : User
    {
        $this->passwordHash = $passwordHash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail( string $email ) : User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagePath() : string
    {
        return $this->imagePath;
    }

    /**
     * @param mixed $image_path
     * @return User
     */
    public function setImagePath( string $imagePath ) : User
    {
        $this->imagePath = $imagePath;
        return $this;
    }

    /**
     * Method to serialize the objects data for string storage.
     * @return string
     */
    public function serialize(  )
    {
        return serialize(
            [
                $this->id,
                $this->username,
                $this->passwordHash,
                $this->email,
                $this->imagePath,
                $this->authenticated
            ]
        );
    }

    /**
     * Function to unserialize this object from storage.
     * @param string $serializedUserString
     */
    public function unserialize( $serializedUserString )
    {
        list(
            $this->id,
            $this->username,
            $this->passwordHash,
            $this->email,
            $this->imagePath,
            $this->authenticated
            ) = unserialize( $serializedUserString );
    }
}
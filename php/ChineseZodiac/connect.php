<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 25-11-2016 18:50
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

$db_name = "ChineseZodiac";
//assign the connection and selected database to a variable
$DBConnect = mysqli_connect( "127.0.0.1", "root", "toor" );
if ( $DBConnect === false )
{
    echo "<p>Unable to connect to the database server.</p>"
        . "<p>Error code " . mysqli_connect_errno() . ": "
        . mysqli_connect_error() . "</p>";
}
else
{
//select the database
    $db = mysqli_select_db( $DBConnect, $db_name );

    if ( $db === false )
    {
        echo "<p>Unable to connect to the database server.</p>"
            . "<p>Error code " . mysqli_connect_errno() . ": "
            . mysqli_connect_error() . "</p>";
        mysqli_close( $DBConnect );
        $DBConnect = false;
    }
}
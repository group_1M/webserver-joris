<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 10-10-2016 ${HOURS}:${MINUTES}
 */

spl_autoload_register( function( $className ){
    $vendorPrefix = 'JorisRietveld\\';

    if( strpos( $className, $vendorPrefix ) !== 0 )
    {
        // class not from this vendor move to next registered autoloader.
        return;
    }

    // Get the base directory of the project. Something like /var/www/www.project.com/
    $baseDir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'RssAndAtom' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;

    // Translate the full class name to an file on the file system.
    $classPath = str_replace( [ $vendorPrefix, '\\'], [$baseDir, DIRECTORY_SEPARATOR], $className ) . '.php';
    require $classPath;
});



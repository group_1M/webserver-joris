<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 28-09-2016 13:49
 */
declare(strict_types = 1);

namespace JorisRietveld\Website\Controllers;


use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\Response;
use JorisRietveld\Website\Core\BaseController;

class Home extends BaseController implements ControllerContract
{
    public function index() : Response
    {
        $response = new Response();

        $sql = 'SELECT * FROM bug_reports';

        $statement = $this->getConnection()->prepare( $sql );

        if( !$statement->execute() )
        {
            throw new \Exception( 'Can\t execute the following statement: <code>' . $sql . '</code>' );
        }

        $response->setContent( $this->renderWebpage( 'home', [ 'bugReports' => $statement->fetchAll() ] ), 200 );

        return $response;
    }
}
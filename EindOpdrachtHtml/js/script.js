/**
 * Created by ubuntu on 9-10-16.
 */

var loadWebpage = function( name ) {
    // Craft an url with the name of the html file passed in this function.
    //var url = 'http://146.185.141.142/joris/EindOpdrachtHtml/pages/' + name + '.html';
    var url = "http://loc.extern/EindOpdrachtHtml/pages/"+name+".html";
    // Create new XMLHTTPRequest so we can make an ajax call to get the html pages.
    var xmlhttp = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

    // Hook an callback that chances includes the file from the ajax call in the webpage.
    xmlhttp.onreadystatechange = function () {
      if(this.readyState == 4 && this.status == 200){
        document.getElementById('content-holder').innerHTML = this.responseText;
      }
    };

    // Do the actual call to get the file.
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
};
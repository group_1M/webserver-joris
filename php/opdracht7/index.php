<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 21-11-2016 17:27
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="UTF-8"/>
    <title>Guest book</title>
</head>
<body>
    <h2>Enter your name to sign our guest book</h2>
    <form method="POST" action="SignGuestBook.php">
        <p>
            <label for="first_name">First Name</label>
            <input type="text" name="first_name" id="first_name"/>
        </p>
        <p>
            <label for="last_name">Last Name</label>
            <input type="text" name="last_name" id="last_name"/>
        </p>
        <p>
            <input type="submit" value="Submit"/>
        </p>
    </form>
    <p>
        <a href="ShowGuestBook.php">Show Guest Book</a>
    </p>

</body>
</html>

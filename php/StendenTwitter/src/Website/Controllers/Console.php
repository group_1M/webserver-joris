<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 27-11-2016 15:10
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */
declare( strict_types = 1 );

namespace JorisRietveld\Website\Controllers;

use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\Response;

class Console extends BaseController implements ControllerContract
{
    /**
     * @return mixed
     */
    public function index()
    {
        return new Response(
            $this->renderWebpage( 'error404' ),
            404
        );
    }

}
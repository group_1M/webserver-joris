<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 26-11-2016 09:40
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Interfaces;


use JorisRietveld\Website\Helper\WhereClause;

interface RepositoryInterface
{
    public function getAll() : array;

    public function getById( int $id ) : EntityInterface;

    public function update( int $id, array $updateValues = [] ) : bool;

    public function delete( int $id ) : bool;

    public function insert( EntityInterface $entity ) : bool;

    //public function getAllWithFilter( WhereClause $whereClause ) : array;
}
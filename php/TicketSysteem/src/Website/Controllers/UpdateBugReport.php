<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 21-11-2016 19:34
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Controllers;


use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\Response;

class UpdateBugReport extends BaseController implements ControllerContract
{
    protected $getReport = 'SELECT `id`, `product_name`, `product_version`, `hardware`, `operating_system`, `frequensy`, `solutions` 
                            FROM bug_reports WHERE `id` = :id';

    protected $updateReport = 'UPDATE bug_reports SET 
                               `product_name` = :product_name, 
                               `product_version` = :product_version,
                               `hardware` = :hardware,
                               `operating_system` = :operating_system,
                               `frequensy` = :frequensy,
                               `solutions` = :solutions
                               WHERE `id` = :id';

    protected $requiredPostFields = [
        'id',
        'product_name',
        'product_version',
        'hardware',
        'operating_system',
        'frequensy',
        'solutions'
    ];


    public function index( $bugReportId = 0, $messages = 'hello world' ) : Response
    {
        try
        {
            return new Response(
                $this->renderWebpage(
                    'updateBugReport', [
                        'formdata' => $this->getBugReportFromDatabase( (int)$bugReportId ),
                        'message' => $messages,
                    ]
                )
            );
        }
        catch ( \Exception $e )
        {
            return new Response(
                $this->renderWebpage(
                    'updateBugReport', [
                        'message' => $e->getMessage()
                ] )
            );
        }
    }

    public function handleForm(  ) : Response
    {
        foreach ($this->requiredPostFields as $requiredPostField)
        {
            if ( !array_key_exists( $requiredPostField, $_POST ) )
            {
                return $this->index( $_POST['id'], '<b class="text-danger">You must fill in all fields also:' . $requiredPostField . '</b>' );
            }
        }
        try
        {
            $connection = $this->getConnection();

            $statement = $connection->prepare( $this->updateReport );

            foreach ($_POST as $columnKey => $postValue)
            {
                if ( in_array( $columnKey, $this->requiredPostFields ) )
                {
                    $statement->bindValue( ':'.$columnKey, $postValue );
                }
            }

            if ( $statement->execute() )
            {
                return $this->index( $_POST['id'], '<b class="text-success">Your bug report has been updated</b>' );
            }
        } catch ( \PDOException $e )
        {
            return $this->index( $_POST['id'], '<b class="text-danger">There was an error updating your bug report in the database.</b><br>'.$e->getMessage() );
        }
    }

    protected function getBugReportFromDatabase( int $id )
    {
        $statement = $this->getConnection()->prepare( $this->getReport );

        if( $statement->execute( [ ':id' => $id ] ) )
        {
            return $statement->fetchAll()[0];
        }
        throw new \Exception( 'Can not select the bug report from the database.' );
    }
}
<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 17-11-2016 09:17
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */
class ErrorMessage
{
    public static $messages = [];

    public static function setMessage( $message )
    {
        self::$messages[] = $message;
    }

    public static function getMessages(  ) : array
    {
        return self::$messages;
    }
}

function parseZodiacSigns( $userInput ) : array
{
    $zodiacArray = explode( ',', $userInput );

    if( count($zodiacArray) < 12 )
    {
        ErrorMessage::setMessage( 'I asked you to type 12 zodiac signs, can\'t count?');
    }

    sort( $zodiacArray );
    return $zodiacArray;
}

function handleRequest( ) : string
{
    ob_start();

    if( isset( $_POST['zodiac-signs']))
    {
        $zodiacString = htmlentities( $_POST['zodiac-signs'], ENT_QUOTES, 'UTF-8' );
        $zodiacArray = parseZodiacSigns( $zodiacString );

        if( count( ErrorMessage::getMessages() ))
        {
            var_dump( ErrorMessage::getMessages());
        }
        else
        {
            echo '<table><thead><tr><th>Zodiac signs</th></tr></thead><tbody>';
            foreach ( $zodiacArray as $zodiacSign )
            {
                printf('<tr><td>%s</td></tr>', $zodiacSign);
            }
            echo '</tbody></table>';
        }
    }

    return ob_get_clean();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Opdracht 4</title>
    <style>
        table > tbody > tr:nth-child(even){
            background-color: #f1f1f1f1;
        }
    </style>
</head>
<body>
    <h1>Opdracht 4</h1>

    <form method="post" action="opdracht4.php">

        <label for="zodiac-signs">Type 12 zodiac signs separated by commas</label>
        <br />
        <textarea name="zodiac-signs" id="zodiac-signs" minlength="50" required></textarea>
        <br />
        <button type="submit">Let magic sort your zodiac signs</button>
    </form>
    <?= handleRequest() ?>

    <fieldset id="code">
        <legend>Source code: Opdracht4.php</legend>
        <?= highlight_file('opdracht4.php', true ) ?>
    </fieldset>
</body>
</html


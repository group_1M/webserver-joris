<?php

/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 21-11-2016 19:17
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */
class Database
{
    protected $connection;
    protected $dbSettings;

    public function __construct( $dbSettings = [] )
    {
        $this->dbSettings = $dbSettings ?? [
            'driver' => 'mysql',
                'dbname' => 'php2',
                'host' => '127.0.0.1',
                'port' => '3306',
                'username' => 'root',
                'password' => 'toor',
                'settings' => [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false,
                ]
            ];
    }

    public function connect() : \PDO
    {
        $this->connection = new \PDO( 'mysql:dbname=;host=127.0.0.1;port=3066', 'root', 'toor');

        return $this->connection;
    }

    public function getBugReports()
    {

    }

    public function setBugReport()
    {

    }

    public function updateBugReport()
    {

    }

    public function getBugReport( int $id )
    {

    }

    public function installDatabase()
    {
        $sql = "
            CREATE TABLE bug_reports(
              `id` INT UNSIGNED UNIQUE PRIMARY KEY AUTO_INCREMENT,
              `product_name` VARCHAR(100),
              `product_version` VARCHAR(10),
              `hardware` VARCHAR(255),
              `operating_system` VARCHAR(255),
              `frequensy` VARCHAR(100),
              `solutions` TEXT
            );";
    }
}
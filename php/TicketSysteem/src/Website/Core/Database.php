<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 21-11-2016 20:23
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Core;


use Symfony\Component\Console\Exception\LogicException;

class Database
{
    protected $connection;
    protected $dbSettings;

    /**
     * Database constructor.
     *
     * @param array $dbSettings
     *
     * todo finish this class, write proper error handling and settings checking, and implement more drivers
     */
    public function __construct( $dbSettings = [] )
    {
        $this->dbSettings = count( $dbSettings ) > 4 ? $dbSettings : [
                'driver' => 'mysql',
                'dbname' => 'php2',
                'host' => '127.0.0.1',
                'port' => '3306',
                'username' => 'root',
                'password' => 'toor',
                'settings' => [
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_EMULATE_PREPARES => false,
                ]
            ];
    }

    /**
     * @return \PDO
     */
    public function connect() : \PDO
    {
        $this->connection = new \PDO( $this->getDsnString(), $this->dbSettings['username'], $this->dbSettings['password'], $this->dbSettings['settings'] );

        return $this->connection;
    }

    /**
     * @return string
     */
    public function getDsnString(  ) : string
    {
        extract( $this->dbSettings, EXTR_OVERWRITE );

        switch ( $this->dbSettings['driver'] )
        {
            case 'mysql':
                return "$driver:host=$host;dbname=$dbname;port=$port";

            default:
                throw new LogicException('The driver specified is not supported.');
        }
    }


    /*public function installDatabase()
    {
        $sql = "
            CREATE TABLE bug_reports(
              `id` INT UNSIGNED UNIQUE PRIMARY KEY AUTO_INCREMENT,
              `product_name` VARCHAR(100),
              `product_version` VARCHAR(10),
              `hardware` VARCHAR(255),
              `operating_system` VARCHAR(255),
              `frequensy` VARCHAR(100),
              `solutions` TEXT
            );";
    }*/
}
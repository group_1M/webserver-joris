<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 28-09-2016 13:49
 */
declare(strict_types = 1);

namespace JorisRietveld\Website\Controllers;


use JorisRietveld\Website\Entity\Message;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\Response;
use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class Home extends BaseController implements ControllerContract
{
    protected $user;

    public function __construct( Request $request )
    {
        parent::__construct();

        $this->setRequest( $request );

        if ( $this->getRequest()->getSession()->has( 'user' ) )
        {
            $this->user = unserialize( $this->getRequest()->getSession()->get( 'user' ) );
        }
        else
        {
            $this->user = new User();
        }

        $this->user->setRepository(
            new \JorisRietveld\Website\Repository\User( $this->getConnection() )
        );
    }

    public function index() : Response
    {
        return new Response(
            $this->renderWebpage( 'home'),
            200
        );
    }

    public function handleTweet(  )
    {
        if( $this->getRequest()->request->has('message') )
        {
            $message = new Message();
        }
    }
}
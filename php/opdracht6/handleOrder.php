<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 19-11-2016 14:47
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

$articles = [
    [
        'name' => 'Lorhoja',
        'Description' => 'Kist van 4',
        'Price' => 14.95,
    ],
    [
        'name' => 'Lagis',
        'Description' => 'Muismat',
        'Price' => 0.99,
    ],
    [
        'name' => 'Skribent',
        'Description' => 'Boekensteun',
        'Price' => 3.99,
    ],
    [
        'name' => 'Moppe',
        'Description' => 'Miniladekast',
        'Price' => 11.95,
    ],
    [
        'name' => 'Dokument',
        'Description' => 'Pennekoker',
        'Price' => 1.99,
    ],
];

$renderForm = function () use ( $articles ) : string
{
    $output = <<<HTML
    <form action="opdracht6.php" method="post">
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th></th>
                    <th>Quantity</th>
                    <th></th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
HTML;

    foreach ($articles as $article)
    {
        $output .= '<tr>';
        $output .= '<td>' . $article[ 'name' ] . '</td>';
        $output .= '<td>' . $article[ 'Description' ] . '</td>';
        $output .= '<td class="price">' . $article[ 'Price' ] . '</td>';
        $output .= '<td>X</td>';
        $output .= '<td>';
        $output .= '<label for="quantity-' . $article[ 'name' ] . '"></label>';
        $output .= '<input onchange="updatePrices();" class="quantity" name="quantity-' . $article[ 'name' ] . '" id="quantity-' . $article[ 'name' ] . '" type="number" value="0" step="any" min="0" />';
        $output .= '</td>';
        $output .= '<td>=</td>';
        $output .= '<td>&euro; <span class="total">0.00</span></td>';
        $output .= '</tr>';
    }

    $output .= <<<HTML
                <tr>
                    <td colspan="8">
                        <span class="pull-right"id="total">&euro; 0.00</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <button class="pull-right" type="button" onclick="updatePrices();">Update</button>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <button class="pull-right"  type="submit">Submit</button>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" id="feedback"></td>
                <tr>
            </tbody>
        </table>
    </form>
HTML;
    return $output;
};

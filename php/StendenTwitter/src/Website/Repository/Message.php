<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 26-11-2016 09:52
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Repository;


use JorisRietveld\Website\Interfaces\EntityInterface;
use JorisRietveld\Website\Interfaces\RepositoryInterface;

class Message implements RepositoryInterface
{
    protected $lastInsertedId;
    /**
     * @return array
     */
    public function getAll() : array
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param int $id
     * @return EntityInterface
     */
    public function getById( int $id ) : EntityInterface
    {
        // TODO: Implement getById() method.
    }

    /**
     * @param int   $id
     * @param array $updateValues
     * @return bool
     */
    public function update( int $id, array $updateValues = [] ) : bool
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete( int $id ) : bool
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function insert( EntityInterface $entity ) : bool
    {
        // TODO: Implement insert() method.
    }

}
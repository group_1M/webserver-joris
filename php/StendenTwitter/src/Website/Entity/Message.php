<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 26-11-2016 09:48
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Entity;


use JorisRietveld\Website\Interfaces\EntityInterface;
use JorisRietveld\Website\Repository\Message as MessageRepository;

class Message implements EntityInterface
{
    protected $id;
    protected $user_id;
    protected $message;
    protected $repository;

    public function __construct( int $id = -1, int $userId = -1, string $message = '' )
    {
        $this->setId( $id );
        $this->setUserId( $userId );
        $this->setMessage( $message );
        $this->repository = new MessageRepository();
    }

    public function setRepository( MessageRepository $messageRepository )
    {
        $this->repository = $messageRepository;
    }

    public function insert(  )
    {
        $this->repository->insert( $this );
    }

    public function send(  )
    {
        $this->repository->insert( $this );
    }

    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Message
     */
    public function setId( int $id ) : Message
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId() : int
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Message
     */
    public function setUserId( int $user_id ) : int
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return Message
     */
    public function setMessage( string $message ) : Message
    {
        $this->message = $message;
        return $this;
    }


}
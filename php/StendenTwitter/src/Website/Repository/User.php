<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 26-11-2016 09:39
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Repository;


use JorisRietveld\Website\Helper\WhereClause;
use JorisRietveld\Website\Interfaces\EntityInterface;
use JorisRietveld\Website\Interfaces\RepositoryInterface;
use JorisRietveld\Website\Entity\User as UserEntity;

class User
{
    protected $databaseConnection;
    protected $lastInsertedId;

    protected $getAllSql = '
        SELECT 
        `id`, 
        `username`, 
        `password` AS passwordHash, 
        `email`, 
        `image_path` AS imagePath
        FROM `StendenTwitter`.`stenden_users` 
    ';

    protected $insertSql = '
        INSERT INTO `StendenTwitter`.`stenden_users`( username, password, email, image_path ) 
        VALUES ( :username, :passwordHash, :email, :imagePath )
    ';

    public function __construct( \PDO $databaseConnection )
    {
        $this->databaseConnection = $databaseConnection;
    }


    /**
     * @return array
     */
    public function getAll() : array
    {
        return $this->getAllWithFilter( '`StendenTwitter`.`id`>:id', [ ':id' => 0 ] );
    }

    /**
     * @param int $id
     * @return EntityInterface
     */
    public function getById( int $id ) : EntityInterface
    {
        return $this->getAllWithFilter( '`StendenTwitter`.`id`=:id', [ ':id' => $id ] );
    }

    /**
     * @param int   $id
     * @param array $updateValues
     * @return bool
     */
    public function update( int $id, array $updateValues = [] ) : bool
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete( int $id ) : bool
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function insert( \JorisRietveld\Website\Entity\User $entity ) : int
    {
        $statement = $this->databaseConnection->prepare( $this->insertSql );

        $data = [
            'username' => $entity->getUsername(),
            'passwordHash' => $entity->getPasswordHash(),
            'email' => $entity->getEmail(),
            'imagePath' => $entity->getImagePath(),
        ];

        $success = $statement->execute( $data );

        if( $success )
        {
            return $this->databaseConnection->lastInsertId();
        }
        else
        {
            throw new \Exception( 'The user could not be inserted.' );
        }
    }

    /**
     * @param $whereClause
     * @return array
     */
    public function getAllWithFilter( string $whereClause, array $boundValues ) : array
    {
        $statement = $this->databaseConnection->prepare( $this->getAllSql . ' WHERE ' . $whereClause );
        $statement->execute( $boundValues );

        $results = $statement->fetchAll( \PDO::FETCH_ASSOC );
        $users = [];

        foreach ( $results as $result )
        {
            $user = new UserEntity();
            $user->setId( $result['id'] );
            $user->setUsername( $result['username'] );
            $user->setEmail( $result['email'] );
            $user->setImagePath( $result['imagePath'] );
            $user->setPasswordHash( $result['passwordHash'] );
            $users[] = $user;
        }

        return $users;
    }
}
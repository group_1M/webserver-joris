/**
 * Created by ubuntu on 19-11-16.
 */

String.prototype.rtrim = function(s) {
  return this.replace(new RegExp(s + "*$"),'');
};

var updatePrices = function () {
  try {
    var prices = document.getElementsByClassName('price');
    var quantity = document.getElementsByClassName('quantity');
    var totals = document.getElementsByClassName('total');
    var totalSumElement = document.getElementById( 'total' );
    var totalSum = 0;

    for (var iteration = 0; iteration < prices.length; iteration++) {
      totalPrice = parseFloat( prices[iteration].innerHTML ) * quantity[iteration].value;
      totals[ iteration ].innerHTML = totalPrice.toFixed(2);
      totalSum += totalPrice;
    }
    totalSumElement.innerHTML = '&euro; ' + totalSum.toFixed(2);

  }
  catch ( error ){
    console.log('An error has occurred: ' + error );
  }
};

window.addEventListener( 'load', function () {

  function sendData() {

    var XHR = new XMLHttpRequest();

    var quantity = document.getElementsByClassName('quantity');
    var formData = '';

    for (var iteration = 0; iteration < quantity.length; iteration++)
    {
      formData += encodeURIComponent( quantity[ iteration ].name ) + '=' + encodeURIComponent( quantity[ iteration ].value ) + '&';
    }
    formData = formData.rtrim('&');

    XHR.addEventListener( 'load', function ( event ) {
      // Display the feedback and reset the form.
      document.getElementsByTagName('form')[0].reset();
      document.getElementById( 'feedback' ).innerHTML = '<span class="success">' + event.target.responseText + '</span>';
      updatePrices();
    } );

    XHR.addEventListener( 'error', function ( event ) {
      // Display an error message.
      document.getElementById( 'feedback' ).innerHTML = '<span class="error">Oups! Something goes wrong. ' + XHR.statusText + '</span>';
    } );

    XHR.open( 'POST', 'http://loc.extern/php/opdracht6/handle.php' );
    XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    XHR.send( formData );
  }

  var form = document.getElementsByTagName('form')[0];

  form.addEventListener( 'submit', function ( event ) {
    event.preventDefault();

    var quantity = document.getElementsByClassName('quantity');
    var totalItems = 0;

    for( var i = 0; i < quantity.length; i++ )
    {
      totalItems += quantity[i].value;
    }

    if( totalItems > 0 ){
      sendData();
    }else{
      document.getElementById( 'feedback' ).innerHTML = '<span class="error">You should at least order 1 item</span>';
    }

  });

} );

<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 25-11-2016 19:46
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

include "connect.php";

function getPosts( $dbLink ) : Traversable
{
    return mysqli_query( $dbLink, 'SELECT * FROM zodiac_feedback' );
}


?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="UTF-8"/>
    <title>Zodiac signs</title>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
</head>
<body>
<main class="container">

    <div class="jumbotron">
        <table class="table">
            <thead>
            <tr>
                <td>Date</td>
                <td>Time</td>
                <td>Sender</td>
                <td>Message</td>
                <td>Public</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach (getPosts( $DBConnect ) as $row)
            {
                echo '<tr>';
                echo '<td>';
                echo $row[ 'message_date' ];
                echo '</td>';
                echo '<td>';
                echo $row[ 'message_time' ];
                echo '</td>';
                echo '<td>';
                echo $row[ 'sender' ];
                echo '</td>';
                echo '<td>';
                echo $row[ 'message' ];
                echo '</td>';
                echo '<td>';
                echo ( $row[ 'public_message' ] === 'Y' ) ? 'Yes' : 'No';
                echo '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <a href="zodiac_feedback.php">Home</a>
    </div>
</body>
</html>


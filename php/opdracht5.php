<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 19-11-2016 03:15
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

function generatePhotoArray( string $imageDirectory = '', array $allowedFileExtensions = [ 'jpg','png' ])
{
    if ( strlen( $imageDirectory ) < 1 )
    {
        $imageDirectory = __DIR__ . DIRECTORY_SEPARATOR . 'zodiac';
    }

    $directory = new RecursiveDirectoryIterator( $imageDirectory );
    $filter = new RecursiveCallbackFilterIterator(
        $directory,
        function ( $current, $key, $iterator ) use ( $allowedFileExtensions )
        {

            if ( $iterator->hasChildren() )
            {
                return true;
            }

            if ( in_array( $current->getExtension(), $allowedFileExtensions, false ) )
            {
                return true;
            }

            return false;
        }
    );

    $returnArray = [];

    foreach ( new RecursiveIteratorIterator( $filter )  as $file)
    {
        $returnArray[ 'zodiac' . DIRECTORY_SEPARATOR . $file->getFileName() ] = 'Image of the zodiac sign ' . substr( $file->getFileName(), 0, -strlen( $file->getExtension()) );
    }

    return $returnArray;
}

function generatePhotoGallery( array $photoArray = [] ) : string
{
    $output = '';
    $photoArray = count( $photoArray ) ? $photoArray : generatePhotoArray();

    foreach ( $photoArray as $photoSrc => $photoCaption )
    {
        $output .= sprintf( '<a href="%s">', $photoSrc );
        $output .= '<figure class="photo-wrapper col-3">';
        $output .= sprintf( '<img src="%s" alt="%s" class="photo center" />', $photoSrc, $photoCaption );
        $output .= sprintf( '<figcaption>%s</figcaption>', $photoCaption );
        $output .= '</figure>';
        $output .= '</a>';
    }

    return $output;
}


?>

<!DOCTYPE html>
<html>
<head>
    <title>Opdracht 5</title>
    <link href="zodiac/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <h1>Opdracht 5</h1>

    <div class="galary-wrapper container">
        <?= generatePhotoGallery() ?>
    </div>
    <fieldset id="code">
        <legend>Source code: Opdracht5.php</legend>
        <?= highlight_file( 'opdracht5.php', true ) ?>
    </fieldset>
</body>
</html


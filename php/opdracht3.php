<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 17-11-2016 09:17
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */


if (isset($_GET['action']))
{
    if ( (file_exists("SongOrganizer/songs.txt")) && ( filesize( "SongOrganizer/songs.txt" ) != 0 ))
    {
        $SongArray = file("SongOrganizer/songs.txt");

        switch ($_GET['action'])
        {
            case 'Remove Duplicates':
                $SongArray = array_unique( $SongArray );
                $SongArray = array_values( $SongArray );
                break;

            case 'Sort Ascending':
                sort( $SongArray );
                break;

            case 'Sort Descending':
                rsort( $SongArray );
                break;

            case 'Shuffle':
                shuffle($SongArray);
                break;

            case 'delFirst':
                unset($SongArray[0]);
                break;

            case 'delLast':
                array_pop($SongArray);
                break;

            default:
                trigger_error('Action not found.');
            break;
        }

        if (count($SongArray)>0)
        {
            $NewSongs = implode($SongArray);
            $SongStore = fopen("SongOrganizer/songs.txt","wb");

            if ($SongStore === false)
            {
                echo "There was an error updating the song file\n";
            } else {
                fwrite($SongStore, $NewSongs);
                fclose($SongStore);
            }
        } else {
            unlink("SongOrganizer/songs.txt");
        }

    }
}

if (isset($_POST['submit']))
{
    if( strlen( $_POST['SongName'] ) < 1 || strlen( $_POST['songArtist'] ) < 1 )
    {
        echo 'The song field must not be empty.';
    }
    else
    {
        $SongToAdd = stripslashes( $_POST[ 'songArtist' ] .'-'. $_POST[ 'SongName'] ) . "\n";
        $ExistingSongs = array();
        if ( file_exists( "SongOrganizer/songs.txt" ) && filesize( "SongOrganizer/songs.txt" ) > 0 )
        {
            $ExistingSongs = file( "SongOrganizer/songs.txt" );
            if ( in_array( $SongToAdd, $ExistingSongs ) )
            {
                echo "<p>The song you entered already exists!<br>\n";
                echo "Your song was not added to the list.</p>";
            }
            else
            {
                $SongFile = fopen( "SongOrganizer/songs.txt", "ab" );
                if ( $SongFile === false )
                {
                    echo "There was an error saving your message!\n";
                }
                else
                {
                    fwrite( $SongFile, $SongToAdd );
                    fclose( $SongFile );
                    echo "Your song has been added to the list.\n";
                }
            }
        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Opdracht 3</title>
    </head>
    <body>
    <h1>Opdracht 3</h1>
    <?php
    if ((!file_exists("SongOrganizer/songs.txt")) || (filesize("SongOrganizer/songs.txt") == 0))
    {
        echo "<p>There are no songs in the list.</p>\n";
    }
    else
    {
        $SongArray = file("SongOrganizer/songs.txt");
        echo '<table border="1" width="50%" style="background-color:lightgray">';
        echo "<th>Song name</th><th>Song Artist</th> ";

        foreach ($SongArray as $songRow)
        {
            $songParts = explode( '-', $songRow );
            $songArtist = $songParts[0] ?? 'artist undefined';
            $songName = $songParts[1] ?? 'song name undefined';

            echo '<tr>';
            echo '<td>' . htmlentities( $songName ) .'</td>';
            echo '<td>' . htmlentities( $songArtist ) .'</td>';
            echo '</tr>';
        }
        echo "</table>\n";
    }
    ?>
    <p>
        <a href="opdracht3.php?action=Sort%20Ascending">
            Sort Song List
        </a>
        <br />
        <a href="opdracht3.php?action=Remove%20Duplicates">
            Remove Duplicate Songs
        </a>
        <br />
        <a href="opdracht3.php?action=Shuffle">
            Randomize Song list
        </a>
        <br />
        <a href="opdracht3.php?action=Sort%20Descending">
            Sort descending
        </a>
        <br />
        <a href="opdracht3.php?action=delLast">
            remove last song
        </a>
        <br />
        <a href="opdracht3.php?action=delFirst">
            remove first song
        </a>
    </p>

    <form action="opdracht3.php" method="post">
        <p>
            <b>Add a New Song</b>
        </p>
        <p>
            Song Name: <input type="text" name="SongName" required />
            Song Artist: <input type="text" name="songArtist" required />
        </p>
        <p>
            <input type="submit" name="submit" value="Add Song to List" />
            <input type="reset" name="reset" value="Reset Song Name" />
        </p>
    </form>

    <fieldset id="code">
        <legend>Source code: Opdracht3.php</legend>
        <?= highlight_file('opdracht3.php', true ) ?>
    </fieldset>
    </body>
</html



<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 23-11-2016 00:27
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Controllers;

use JorisRietveld\Website\Core\BaseController;
use JorisRietveld\Website\Interfaces\ControllerContract;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Survey extends BaseController implements ControllerContract
{
    protected $getSurveys = '
        SELECT
          `survey`.`id`,
          `survey`.`friendliness_staff`,
          `survey`.`luggage_space`,
          `survey`.`comfort_seating`,
          `survey`.`cleanlines_arcraft`,
          `survey`.`noice_level`,
          `flight`.`flight_orgin`,
          `flight`.`flight_destenation`
        FROM `survey` JOIN `flight` ON `survey`.`flight_id` = `flight`.`id`';

    protected $insertSurvey = '
        INSERT INTO `survey`(
            `friendliness_staff`,
            `luggage_space`,
            `comfort_seating`,
            `cleanlines_arcraft`,
            `noice_level`,
            `flight_id`
        ) VALUES (
            :friendliness_staff,
            :luggage_space,
            :comfort_seating,
            :cleanlines_arcraft,
            :noice_level,
            :flight_id
        );
        ';

    protected $getFlightOrigins = '
        SELECT DISTINCT `flight`.`flight_orgin` FROM `flight` ;
    ';

    protected $getFlightDestinations = '
        SELECT DISTINCT `flight`.`flight_destenation` FROM `flight` WHERE `flight`.`flight_orgin`=:origin;
    ';

    protected $getFlightsByTime = '
        SELECT `flight`.`flight_time`, `flight`.`id` FROM `flight` WHERE `flight_orgin`=:flight_origin AND `flight_destenation`=:flight_destenation;
    ';

    protected $getResult = '
        SELECT 
        ROUND( AVG( `survey`.`friendliness_staff` ),1) AS `friendliness_staff`,
        ROUND( AVG( `survey`.`cleanlines_arcraft` ),1) AS `cleanlines_arcraft`,
        ROUND( AVG( `survey`.`comfort_seating` ),1) AS `comfort_seating`,
        ROUND( AVG( `survey`.`luggage_space` ),1) AS `luggage_space`,
        ROUND( AVG( `survey`.`noice_level` ),1) AS `noice_level`
        FROM `survey`
    ';

    protected $requiredPostFields = [
        'friendliness_staff',
        'luggage_space',
        'comfort_seating',
        'cleanlines_arcraft',
        'noice_level',
        'flight_id'
    ];

    public function index()
    {

        return new Response(
            $this->renderWebpage( 'viewSurvery', [
                'surveys' => $this->getSurveys(),
                'overall' => $this->getResult(),
            ] ),
            200
        );
    }

    public function insertSurvey()
    {
        $message ='';
        if ( count( $_POST ) > 0 )
        {
            $message = $this->insertSurveyDatabase();
        }
        return new Response(
            $this->renderWebpage( 'insertSurvey', [
                'message' => $message,
                'flightOrigins' => $this->getAllFlightOrigins()
            ] ),
            200
        );
    }

    public function getDestination()
    {
        if ( isset( $_POST[ 'origin' ] ) )
        {
            return new JsonResponse(
                $this->getAllFlightDestinations( $_POST[ 'origin' ] )
            );
        }
        else
        {
            return new Response( 'Error the origin field is not specified', 500 );
        }
    }

    public function getTime()
    {
        if ( isset( $_POST[ 'origin' ], $_POST[ 'destination' ] ) )
        {
            return new JsonResponse(
                $this->getAllFlightTimes( $_POST[ 'origin' ], $_POST[ 'destination' ] )
            );
        }
        else
        {
            return new Response( 'Error the origin field is not specified', 500 );
        }
    }

    protected function getSurveys()
    {
        $statement = $this->getConnection()->prepare( $this->getSurveys );
        $statement->execute();
        return $statement->fetchAll();
    }

    protected function insertSurveyDatabase()
    {
        foreach ($this->requiredPostFields as $requiredPostField)
        {
            if ( !array_key_exists( $requiredPostField, $_POST ) )
            {
                return '<b class="text-danger">You must fill in all fields also:' . $requiredPostField . '</b>';
            }
        }
        try
        {
            $statement = $this->getConnection()->prepare( $this->insertSurvey );

            foreach ($_POST as $columnKey => $postValue)
            {
                if ( in_array( $columnKey, $this->requiredPostFields ) )
                {
                    $statement->bindValue( ':' . $columnKey, $postValue );
                }
            }

            if ( $statement->execute() )
            {
                return '<b class="text-success">Thanks for your feedback.</b>';
            }

        } catch ( \PDOException $e )
        {
            return '<b class="text-danger">There was an error saving your bug in the database.</b><br>' . $e->getMessage();
        }
    }

    protected function getAllFlightOrigins()
    {
        $statement = $this->getConnection()->prepare( $this->getFlightOrigins );
        $statement->execute();

        return $statement->fetchAll( \PDO::FETCH_COLUMN );
    }

    protected function getAllFlightDestinations( string $origin )
    {
        $statement = $this->getConnection()->prepare( $this->getFlightDestinations );
        $statement->execute( [
            ':origin' => $origin
        ] );

        return $statement->fetchAll( \PDO::FETCH_ASSOC );
    }

    protected function getAllFlightTimes( string $origin, string $destination )
    {
        $statement = $this->getConnection()->prepare( $this->getFlightsByTime );
        $statement->execute( [
            ':flight_origin' => $origin,
            ':flight_destenation' => $destination,
        ] );

        return $statement->fetchAll( \PDO::FETCH_ASSOC );
    }

    protected function getResult(  )
    {
        $statement = $this->getConnection()->prepare( $this->getResult );
        $statement->execute();

        return $statement->fetchAll( \PDO::FETCH_ASSOC )[0];
    }
}
<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 17-09-2016 12:12
 */
declare( strict_types = 1 );

namespace JorisRietveld\Website\Core;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\HttpFoundation\Request;
use Website\Exceptions\FileNotFoundException;

abstract class BaseController
{
    /**
     * This property will hold the twig environment that will be used for rendering templates.
     *
     * @var \Twig_Environment
     */
    protected $twigEnv;

    /**
     * This property holds the Doctrine EntityManager for interaction with the database.
     *
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * This property holds the current Request.
     *
     * @var Request
     */
    protected $request;

    protected $database;

    public function __construct()
    {
        $this->loadTemplateEngine();
        //$this->loadEntityManager();
        $this->loadDatabase();
    }

    /**
     * This method loads the entity manager for communication with the database.
     */
    protected function loadEntityManager()
    {
        $databaseConnectionParameters = $this->getConfiguration( 'database_connection', true );

        $entityManagerConfig = Setup::createAnnotationMetadataConfiguration(
            $this->getEntityPaths( 'test' ),
            DEBUG
        );

        $this->entityManager = EntityManager::create(
            $databaseConnectionParameters,
            $entityManagerConfig
        );
    }

    protected function loadDatabase(  )
    {
        $this->database = (new Database())->connect();
    }

    protected function getConnection(  )
    {
        return $this->database;
    }

    protected function loadTemplateEngine()
    {
        $twigLoader = new \Twig_Loader_Filesystem( RESOURCES_DIR . 'twig' . DIRECTORY_SEPARATOR );

        $this->twigEnv = new \Twig_Environment( $twigLoader,
            $this->getConfiguration( 'twig_environment', true )
        );

        $this->twigEnv->addExtension( new \Twig_Extension_Debug() );
    }

    protected function getEntityPaths( string $pathName = null )
    {
        $entityPathConfigurationItems = $this->getConfiguration( 'entity_path', true );
        $entityPaths = [];

        if ( array_key_exists( 0, $entityPathConfigurationItems ) )
        {
            if ( $pathName )
            {
                foreach ($entityPathConfigurationItems as $entityPathConfigurationItem)
                {
                    if ( $entityPathConfigurationItem[ 'name' ] = $pathName )
                    {
                        return [ $entityPathConfigurationItem[ 'path' ] ];
                    }
                }
            }
            else
            {
                foreach ($entityPathConfigurationItems as $entityPathConfigurationItem)
                {
                    $entityPaths[] = $entityPathConfigurationItem[ 'path' ];
                }
            }
        }
        elseif ( array_key_exists( 'name', $entityPathConfigurationItems ) )
        {
            if ( $pathName )
            {
                $entityPaths = ( $entityPathConfigurationItems[ 'name' ] == $pathName ) ? [ $entityPathConfigurationItems[ 'path' ] ] : [];
            }
            else
            {
                $entityPaths = [ $entityPathConfigurationItems[ 'path' ] ];
            }
        }
        return $entityPaths;
    }

    /**
     * @param $fileName
     * @return string
     */
    public function loadedTemplate( $fileName )
    {
        $templateFile = RESOURCES_DIR . 'views' . DIRECTORY_SEPARATOR . $fileName;

        if ( file_exists( $templateFile ) )
        {
            return file_get_contents( $templateFile );
        }
        throw new \Website\Exceptions\FileNotFoundException( 'The template file:' . $fileName . ' is not found!' );
    }

    public function getConfiguration( $for, bool $asArray = false )
    {
        $config = new ConfigLoader();
        return $config->get( $for, $asArray );
    }

    public function renderWebpage( string $webpageName, array $context = [] )
    {
        return $this->twigEnv->render( $webpageName . '.html.twig', $context );
    }


    public function setRequest( Request $request )
    {
        $this->request = $request;
    }

    public function getRequest() : Request
    {
        return $this->request;
    }

    public function getEntityManager() : EntityManager
    {
        return $this->entityManager;
    }


}
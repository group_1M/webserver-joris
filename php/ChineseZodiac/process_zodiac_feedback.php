<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 25-11-2016 18:42
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

function saveMessage( array $posts, $mysqliLink ) :string
{
    try
    {
        $sqlInsert = '
            INSERT INTO `zodiac_feedback`(
                message_date, 
                message_time, 
                sender, 
                message, 
                public_message
            ) 
            VALUES ( ?,?,?,?,? )';

        $requiredPostFields = [
            'message_date',
            'message_time',
            'sender',
            'message',
            'public_message'
        ];

        foreach ($requiredPostFields as $requiredPostField)
        {
            if ( !array_key_exists( $requiredPostField, $posts ) )
            {
                return '<b class="text-danger">You must fill in all fields also:' . $requiredPostField . '</b>';
            }
        }


        $statement = mysqli_prepare( $mysqliLink, $sqlInsert );

        if( !$statement )
        {
            throw new Exception( 'Error:' . mysqli_error( $mysqliLink ) );
        }

        mysqli_stmt_bind_param( $statement, 'sssss', ...array_values( $posts ));

        if ( mysqli_stmt_execute( $statement ))
        {
            return '<b class="text-success">Thanks for your feedback.</b>';
        }

    } catch ( \PDOException $e )
    {
        return '<b class="text-danger">There was an error saving your bug in the database.</b><br>' . $e->getMessage();
    }
}

require __DIR__.DIRECTORY_SEPARATOR.'connect.php';

echo saveMessage( $_POST, $DBConnect );
?>
<a href="zodiac_feedback.php">Home</a>

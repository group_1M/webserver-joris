<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 27-11-2016 03:28
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace JorisRietveld\Website\Interfaces;


abstract class Repository
{
    public function select(  )
    {
        
    }

    public function selectDistinct(  )
    {
        
    }
}